package com.lanit.lanit;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;


public class Issue implements Serializable, IssueRepository {

    public static int sc () {
        return new Scanner(System.in).nextInt();
    }
    public static String scString() {
        return new Scanner(System.in).nextLine();
    }
    private String name;

    public Issue(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    @Override
    public void findAll() {
        System.out.println();
        System.out.println("Вывод всех элементов файла issue:");
        try {
            Files.lines(Paths.get("issue.txt"), Charset.forName("windows-1251")).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save() {
        System.out.println();
        System.out.println("Введите имя issue и нажмите <enter> для записи в файл");
        String insertName = scString();

        try(BufferedWriter bw = new BufferedWriter(new FileWriter("issue.txt", true)))
        {
            String issues = insertName;
            bw.newLine();
            bw.write(issues);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
