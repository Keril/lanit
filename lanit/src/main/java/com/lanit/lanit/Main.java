package com.lanit.lanit;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static int sc () {
        return new Scanner(System.in).nextInt();
    }

    public static void main(String[] args) throws SQLException {

        System.out.println("Нажмите 1 - работа с файловой системой, 2 - работа с БД:");
        try {
            int menu = sc();
            switch (menu) {
                case 1:
                    System.out.println("Работа с файловой системой:");
                    User user1 = new User("user1");
                    user1.save();
                    user1.findAll();

                    Project project1 = new Project("project1");
                    project1.save();
                    project1.findAll();

                    Issue issue1 = new Issue("issue1");
                    issue1.save();
                    issue1.findAll();

                    break;

                case 2:
                    System.out.println();
                    DBOpen dbOpen = new DBOpen();
                    dbOpen.dbStart();
                    Statement stmt = dbOpen.conn.createStatement();

                    dbOpen.createTableUsers();
                    dbOpen.insertUsers();
                    dbOpen.readUsers();

                    dbOpen.createTableProjects();
                    dbOpen.insertProjects();
                    dbOpen.readProjects();

                    dbOpen.createTableIssues();
                    dbOpen.insertIssues();
                    dbOpen.readIssues();

                    stmt.close();
                    break;
                }

            } catch (
                InputMismatchException a) {
                System.out.println("Вы ввели неверные данные. Попробуйте снова!");
                }

    }
}
