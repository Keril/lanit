package com.lanit.lanit;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;


public class Project implements Serializable, ProjectRepository {

    public static int sc () {
        return new Scanner(System.in).nextInt();
    }
    public static String scString() {
        return new Scanner(System.in).nextLine();
    }

    private String name;

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }


    @Override
    public void findAll() {

        System.out.println();
        System.out.println("Вывод всех элементов файла project:");
        try {
            Files.lines(Paths.get("project.txt"), Charset.forName("windows-1251")).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save() {
        System.out.println();
        System.out.println("Введите имя project и нажмите <enter> для записи в файл");
        String insertName = scString();

        try(BufferedWriter bw = new BufferedWriter(new FileWriter("project.txt", true)))
        {
            String projects = insertName;
            bw.newLine();
            bw.write(projects);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
