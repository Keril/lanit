package com.lanit.lanit;

import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class DBOpen {
    public static int sc() {
        return new Scanner(System.in).nextInt();
    }

    public static String scString() {
        return new Scanner(System.in).nextLine();
    }

    public Connection conn = null;
    public Statement stmt = null;

    public void dbStart() throws SQLException {

        try {
            conn = DriverManager.getConnection(
                    "jdbc:h2:C:/user.db");
            System.out.println("Cоединения с БД установлено!");

            if (conn == null) {
                System.out.println("Нет соединения с БД!");
                System.exit(0);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {

            }
        }
    }


    public void createTableUsers() throws SQLException {
        stmt = conn.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS Users " +
                "(user_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                " user_name VARCHAR(255))";
        stmt.executeUpdate(sql);
        System.out.println("Таблица Users создана.");
    }

    public void insertUsers() throws SQLException {
        System.out.println("Введите имя нового user:");
        String insertUserName = scString();
        stmt = conn.createStatement();
        String sql = "INSERT INTO Users (user_name) values ('" + insertUserName + "')";
        stmt.executeUpdate(sql);
        System.out.println();
        System.out.println("В таблицу Users внесены новые данные.");
    }

    public void readUsers () throws SQLException {
        stmt = conn.createStatement();
        String sql = "SELECT user_id, user_name FROM Users";
        ResultSet rs = stmt.executeQuery(sql);
        System.out.println("Вывод таблицы users:");
        System.out.println("id---name");

        try {
            while (rs.next()) {
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                System.out.print(userId);
                System.out.println("   " + userName);
            }
        }catch (InputMismatchException a) {
            System.out.println("Вы ввели неверные данные. Попробуйте снова!");
        }
    }

    public void createTableProjects() throws SQLException {
        stmt = conn.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS Projects " +
                "(user_id INTEGER NOT NULL, " +
                " project_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                " project_name VARCHAR(100)," +
                " FOREIGN KEY (user_id) REFERENCES users(user_id))";
        stmt.executeUpdate(sql);
        System.out.println();
        System.out.println("Таблица Projects создана.");
    }

    public void insertProjects() throws SQLException {
        System.out.println("Введите имя нового project:");
        String insertProjectName = scString();

        System.out.println("Выберите и введите user_id:");
        readUsers();
        int insertUserId = sc();

        String sql = "INSERT INTO Projects (user_id, project_name) values ('" + insertUserId + "', '" + insertProjectName + "')";
        stmt.executeUpdate(sql);
        System.out.println();
        System.out.println("В таблицу Projects внесены новые данные.");
    }

    public void readProjects () throws SQLException {
        stmt = conn.createStatement();
        String sql = "SELECT user_id, project_id, project_name FROM Projects";
        ResultSet rs = stmt.executeQuery(sql);
        System.out.println("Вывод таблицы projects:");
        System.out.println("user_id--project_id--project_name");

        try {
            while (rs.next()) {
                int userId = rs.getInt("user_id");
                int projectId = rs.getInt("project_id");
                String projectName = rs.getString("project_name");

                System.out.print(userId);
                System.out.print("        " + projectId);
                System.out.println("           " + projectName);
            }

        } catch (InputMismatchException a) {
            System.out.println("Вы ввели неверные данные. Попробуйте снова!");
        }
    }

    public void createTableIssues() throws SQLException {
        stmt = conn.createStatement();
        String sql = "CREATE TABLE IF NOT EXISTS Issues " +
                "(user_id INTEGER NOT NULL, " +
                " project_id INTEGER NOT NULL, " +
                " issue_id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT, " +
                " issue_name VARCHAR(100)," +
                " FOREIGN KEY (user_id) REFERENCES users(user_id)," +
                " FOREIGN KEY (project_id) REFERENCES projects(project_id))";
        stmt.executeUpdate(sql);
        System.out.println();
        System.out.println("Таблица Issues создана.");
    }

    public void insertIssues() throws SQLException {
        System.out.println("Введите имя нового issue:");
        String insertIssueName = scString();

        try {
            System.out.println("Выберите и введите user_id:");
            readUsers();
            int insertUserId = sc();

            System.out.println("Выберите и введите project_id:");
            readProjects();
            int insertProjectId = sc();


            String sql = "INSERT INTO Issues (user_id, project_id, issue_name) values ('" + insertUserId + "', '" + insertProjectId + "','" + insertIssueName + "')";
            stmt.executeUpdate(sql);
            System.out.println();
            System.out.println("В таблицу Issues внесены новые данные.");

        } catch (InputMismatchException a) {
        System.out.println("Вы ввели неверные данные. Попробуйте снова!");
        }
    }

    public void readIssues() throws SQLException {
        stmt = conn.createStatement();
        String sql = "SELECT user_id, project_id, issue_id,issue_name FROM Issues";
        ResultSet rs = stmt.executeQuery(sql);
        System.out.println("Вывод таблицы issues:");
        System.out.println("user_id--project_id--issue_id--issue_name");

        while (rs.next()) {
            int userId = rs.getInt("user_id");
            int projectId = rs.getInt("project_id");
            int issueId = rs.getInt("issue_id");
            String issueName = rs.getString("issue_name");

            System.out.print(userId);
            System.out.print("        " + projectId);
            System.out.print("           " + issueId);
            System.out.println("         " + issueName);
        }
    }
}