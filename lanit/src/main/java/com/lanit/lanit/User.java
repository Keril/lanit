package com.lanit.lanit;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;


public class User implements Serializable, UserRepository {

    public static int sc () {
        return new Scanner(System.in).nextInt();
    }
    public static String scString() {
        return new Scanner(System.in).nextLine();
    }

    private String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    @Override
    public void findAll() {

        System.out.println();
        System.out.println("Вывод всех элементов файла user:");
        try {
            Files.lines(Paths.get("user.txt"), Charset.forName("windows-1251")).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void save() {
        System.out.println("Введите имя user и нажмите <enter> для записи в файл");
        String insertName = scString();

        try(BufferedWriter bw = new BufferedWriter(new FileWriter("user.txt", true)))
        {
            String users = insertName;
            bw.newLine();
            bw.write(users);
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
